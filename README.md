# Recommended Reading Cannabis Books
What are the best reading materials for marijuana growing? Tips + Tricks
You can learn everything you need to know about nearly anything on the internet. 
I’m not going to tell you to enroll in a ‘Cannabis University’. That’s the dumbest thing I’ve even seen, and what’s sad is that people actually go to these courses. 
If you really can’t manage your time well enough to schedule in some self study, and would rather go to a fake university to learn about growing weed well just stop right here before we go any further.
Recommended Reading: The Emperor Wears No Clothes by Jack Herer. It’s an old paperback book, but you may be able to find a new print or online version for free.

Okay, so you wanna know about the best reading material for marijuana growing, huh? Well if you want to learn anything I suggest you study about soil microbiology. 
There is an entire fragile ecosystem growing at super-speed in any healthy soil. This is the basis of composting, and why such practices are beneficial.
One common misconception about compost is that it’s added to soil as a nutrient boost for plants. This is wrong. The real reason behind using compost 
and other live organic matter in soils is the huge amounts of microlife that it holds. This microlife will ‘mine’ nutrients from available sources in the soil, 
and in-turn feed them to plant roots in a sort of symbiotic growth relationship.
Recommended reading: https://moldresistantstrains.com/top-8-best-books-marijuana-growing/
That brings us to mycorrhizal fungi. Mycorrhizal fungi is a white fungi that grows symbiotically on the roots of plants. You probably have seen it before as white mold under the mulch layer. 
Top performing organic soils thrive with high levels of mycorrhizal fungi, and marijuana growers have caught on.
Organic agriculture methods typically use long-term release fertilizers for their soils. Fertilizers and additives such as blood meal, bone meal, green sand, oyster shell flower, 
ect. are all examples of long-term release; they are only available to plants when they have been broken down to their smallest particles, or cations.
Recommended Reading: The Marijuana Grow Bible by Jorge Cervantes. Available in hardcover and softcover. Online eBook also for sale.
How to break down long-term release nutrients into cations? The answer is: your soil is doing it as we read this. It’s the natural process of life, and the microbiology in your soil will be breaking down, 
or ‘mining’ available nutrients.
If you want to speed this ‘mining’ process up, then you’ll need to foster a bigger microbiology in the soil. 
This can be done with compost and other practices to ensure that life is not just added, but also thrives where it is added.
By optimizing the aeration and quality of the soil, mycorrhizal fungi and other beneficial microbiology can be grown to encompass a large amount of the cannabis plant’s root space. This is the new hot tip on the market for cannabis growers, which has led to the rise of organic weed being popular and respected as high quality, top-shelf buds.
Recommended Reading: True Living Organics by the Rev, Skunk Magazine. Hardcover and softcover book. eBook possibly available?

